func1 => 3 − exp(x).
func2 => (sin(x))^3 − cos(x).
func3 => 3*x^3 + 2*^2 + x − 15.
func4 => gamma(x + 0.1) − 2.






1. Find the roots of the first three functions with the initial interval [−8, 8].

  Answer : func1 the root is 1.098612
	   func2 the root is 0.972030
           func3 the root is 1.457706

2. Find the roots of all four functions using the initial interval [0, 2].
 
  Answer:  func1 the root is 1.098612
           func2 the root is 0.972030
           func3 the root is 1.457706
	   func4 the root is 0.342877
