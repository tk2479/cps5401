You can find the root of the xth function with initial interval [x_l,x_r] with code below

./solve py_fun.py funcx x_l x_r


Here the funcx can be replaced by func1 which is 3 − exp(x).
				  func2 which is (sin(x))^3 − cos(x).
				  func3 which is 3*x^3 + 2*^2 + x − 15.
				  func4 which is gamma(x + 0.1) − 2.


Here x_l and x_r are the left end and the right end of the initial interval. They are two double values.
