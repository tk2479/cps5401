/*include header*/

#include<stdio.h>
#include<math.h>
#include <tgmath.h>
#include<stdlib.h>
#include <ctype.h>

/*declare functions*/

long double factorial(int a);

void clear_stream(FILE *in);

/*main function*/

int main()

{
  long double x;
  printf("Input the value of 'x'\n");  /*get x*/

  while (scanf("%Lf", &x)!= 1 ){ /*to make sure that x is a numerical value*/
         clear_stream(stdin);
         printf("Ivalid value of x, please try again.\n");  
    }
  clear_stream(stdin);

  long double epsilon;
  printf("Input the value of absolute error tolerance 'epsilon'\n It should be a positive value\n we recommand you to use a number smaller than 10^-6\n To make sure that the accuracy of the estimation will not be too low I used a relative error tolerance 0.01 in case that the epsilon is relatively large\n");   /*get epsilon*/

  while (scanf("%Lf", &epsilon) != 1 || epsilon <=0){ /*to make sure that epsilon is a numerical value*/
         clear_stream(stdin);
         printf("Ivalid value of epsilon, please try again.\n");  
  }

  int n;
  printf("Enter an integer for the index 'n'\n n should be an non-negative integer\n If the number you input is not an integer it will be truncated to make it an integer.\n"); /*get n*/
  
 
  while (scanf("%d", &n) != 1 || n <0){   /*to make sure that n is an non-negative integer*/
         clear_stream(stdin);
         printf("Ivalid value of n, please try again.\n");  
  }

  printf("The n is %d, x is %Le and epsilon is %Le \n",n,x,epsilon);

  int m = 0;

  long double alpha = 0;

  long double b;
  
  long double c = pow(x/2,2*m+n);

  long double d = factorial(n);

  b = c/d;
  
  long double k =1;

  while ((b > epsilon || m < 4 || k > 0.001 || k < -0.001)&& m < 40){
    alpha = alpha + pow(-1,m)*b;

    m = m + 1;

    c = pow(x/2,2*m+n);

    d = factorial(m)*factorial(n+m);

    b = c/d;

    k = b/alpha;
  }
    printf("The estimated value is %Lf \n",alpha);
}

long double factorial(int a)         // factorial function definition
{
      int i;
      long double f = 1;
      if(a == 0){
       	 	f =1;	
		}
      else{
		for(i=1;i<=a;i++)
		{f=f*i;}
          }
      return f;
}

void clear_stream(FILE *in)
{
    int ch;
    
    clearerr(in);
    
    do
        ch = getc(in);
    while (ch != '\n' && ch != EOF);
    
    clearerr(in);
}
